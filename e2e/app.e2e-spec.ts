import { NewstartPage } from './app.po';

describe('newstart App', () => {
  let page: NewstartPage;

  beforeEach(() => {
    page = new NewstartPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
